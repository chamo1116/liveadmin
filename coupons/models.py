from djongo import models


# Create your models here.
class CouponsModels(models.Model):
    expiredDate = models.DateField(verbose_name='Fecha de Vencimiento', null=True, blank=True)
    description = models.CharField(max_length=100, verbose_name='Descripción del cupón')
    number = models.IntegerField(verbose_name='Valor a aplicar(COP)')
    initialStock = models.IntegerField(verbose_name='Cantidad inicial', null=True, blank=True)
    stock = models.IntegerField(verbose_name='Cantidad restante', null=True, blank=True)
    reference = models.CharField(max_length=100, verbose_name='Código del cupón', primary_key=True)
    state = models.BooleanField(verbose_name='Estado', default=True)
    newUsers = models.BooleanField(verbose_name='Aplica para usuarios Nuevos', default=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')

    class Meta:
        ordering = ['-created']
        db_table = 'coupons'
        verbose_name = 'Cupón'
        verbose_name_plural = "Cupones"
