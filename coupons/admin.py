from django.contrib import admin
from .models import CouponsModels


# Register your models here.
class CouponsAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated', 'stock')
    list_display = ('reference', 'number', 'state')
    search_fields = ('state', 'description')
    list_filter = ('state',)


admin.site.register(CouponsModels, CouponsAdmin)