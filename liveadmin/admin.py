from django.contrib import admin


class MyAdminSite(admin.AdminSite):
    site_header = 'Live Admin'
    index_title = "Configuración Artistas"
    site_title = "Live Admin"
