from django.contrib import admin
from .models import ArtistsModels


# Register your models here.
class ArtistAdmin(admin.ModelAdmin):
    readonly_fields = ('created', 'updated', 'rating', 'numberServices', 'serviceId', 'comments')
    list_display = ('post_name', 'post_email', 'post_phone')
    search_fields = ('name', 'email', 'phone')
    list_filter = ('categories',)

    def post_name(self, obj):
        return str(obj.name)

    def post_email(self, obj):
        return str(obj.email)

    def post_phone(self, obj):
        return str(obj.phone)

    post_name.short_description = "Nombre"
    post_email.short_description = "Email"
    post_phone.short_description = "Teléfono"


admin.site.register(ArtistsModels, ArtistAdmin)



