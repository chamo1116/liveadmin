# Generated by Django 2.2.2 on 2019-08-04 15:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('artists', '0003_auto_20190804_1458'),
    ]

    operations = [
        migrations.AlterField(
            model_name='artistsmodels',
            name='description',
            field=models.TextField(blank=True, max_length=200, null=True, verbose_name='Descripción del show'),
        ),
    ]
