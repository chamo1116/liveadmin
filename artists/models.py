from djongo import models
from liveadmin.custom_storage import MediaStorage

typeId = [('CC', 'Cédula Ciudadanía'), ('TI', 'Tarjeta de identidad'), ('CE', 'Cédula extranjería'),
          ('PA', 'Pasaporte')]

categories = [('popular', 'Popular'), ('tropical', 'Tropical'), ('romantica', 'Romantica'), ('baladas', 'Baladas'),
              ('jazz', 'Jazz'), ('rock', 'Rock'), ('electronica', 'Electrónica'), ('pop', 'Pop'),
              ('vallenato', 'Vallenato'), ('mariachi', 'Mariachi'), ('clasica', 'Clásica'), ('urbana', 'Urbana'),
              ('salsa', 'Salsa'), ('bolero', 'Bolero'), ('variado', 'Variado')]

cities = [('cali', 'Cali'), ('medellin', 'Medellin'), ('bogota', 'Bogotá'), ('barranquilla', 'Barranquilla'),
          ('cartagena', 'Cartagena'), ('valledupar', 'Valledupar'), ('santa marta', 'Santa Marta'),
          ('manizales', 'Manizales'), ('pereira', 'Pereira'), ('ibague', 'Ibagué'), ('pasto', 'Pasto')]


# Create your models here.
class ArtistsModels(models.Model):
    email = models.EmailField(max_length=100, verbose_name='Email')
    name = models.CharField(max_length=100, verbose_name='Nombre')
    phone = models.CharField(max_length=100, verbose_name='Celular')
    city = models.CharField(max_length=100, verbose_name='Ciudad', choices=cities)
    description = models.TextField(max_length=800, verbose_name='Descripción del show', null=True, blank=True)
    video = models.CharField(max_length=200, verbose_name='URL video', null=True, blank=True)
    comments = models.CharField(max_length=200, verbose_name='Descripción del show', null=True, blank=True)
    rating = models.IntegerField(verbose_name='Calificación', default=0)
    numberServices = models.IntegerField(verbose_name='Número de servicios', default=0)
    serviceId = models.CharField(max_length=100, verbose_name='Id de Servicio', null=True, blank=True)
    typeIdentification = models.CharField(max_length=100, verbose_name='Tipo de identificación', choices=typeId)
    identification = models.IntegerField(verbose_name='Número de Identificación')
    address = models.CharField(max_length=100, verbose_name='Dirección')
    alone = models.BooleanField(verbose_name='¿Eres Solista?', default=True)
    profilePhoto = models.ImageField(storage=MediaStorage(), verbose_name='Foto de perfil', null=True, blank=True)
    price = models.IntegerField(verbose_name='Precio/Hora')
    categories = models.CharField(max_length=100, verbose_name='Categorías', choices=categories)
    state = models.BooleanField(verbose_name='Estado', default=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Fecha de creación')
    updated = models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')

    class Meta:
        ordering = ['-created']
        db_table = 'artists'
        verbose_name = 'Artista'
        verbose_name_plural = "Artistas"
