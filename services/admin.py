from django.contrib import admin
from .models import ServicesModels


# Register your models here.
class ServicesAdmin(admin.ModelAdmin):
    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
    readonly_fields = ('serviceDate', 'expiredDate', 'hourDate', 'price', 'category', 'artistId', 'userId', 'place',
                       'createdAt', 'updatedAt', 'additionalComment')
    list_display = ('place', 'serviceDate', 'state')
    search_fields = ('place', 'priceRange', 'artistId', 'userId')
    list_filter = ('state', 'artistId', 'userId')


admin.site.register(ServicesModels, ServicesAdmin)