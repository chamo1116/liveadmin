from djongo import models
from bson import ObjectId

states = [('Wait artist confirmation', 'Esperando confirmación del artista'),
          ('Artist Confirmed', 'Artista Confirmado')]


# Create your models here.
class ServicesModels(models.Model):
    serviceDate = models.IntegerField(verbose_name='Fecha de Servicio')
    expiredDate = models.IntegerField(verbose_name='Fecha de Servicio')
    hourDate = models.CharField(max_length=100, verbose_name='Hora de Servicio', null=True, blank=True)
    price = models.CharField(max_length=100, verbose_name='Precio', null=True, blank=True)
    category = models.CharField(max_length=100, verbose_name='Categoría', null=True, blank=True)
    artistId = models.CharField(max_length=100, verbose_name='Id del artista', null=True, blank=True)
    userId = models.CharField(max_length=100, verbose_name='Id del usuario', null=True, blank=True)
    place = models.CharField(max_length=100, verbose_name='Lugar del evento', primary_key=True)
    state = models.CharField(max_length=100, verbose_name='Estado del evento', null=True, blank=True, choices=states)
    additionalComment = models.CharField(max_length=100, verbose_name='Comentarios adicionales', null=True, blank=True)
    createdAt = models.DateTimeField(verbose_name='Fecha de creación', null=True, blank=True)
    updatedAt = models.DateTimeField(auto_now=True, verbose_name='Fecha de edición')

    class Meta:
        ordering = ['-createdAt']
        db_table = 'services'
        verbose_name = 'Servicio'
        verbose_name_plural = "Servicios"
